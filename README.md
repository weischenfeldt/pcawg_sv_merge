# PCAWG SV merge

PCAWG6 workflow to merge SVs from Sanger (BRASS), Broad Inst (DRanger & SnowMan) and EMBL/DKFZ (DELLY)

## Build the image

```
git clone https://ffavero@bitbucket.org/weischenfeldt/pcawg_sv_merge.git
cd pcawg_sv_merge
docker build -t pcawg_sv_merge:latest .
```

## Dockstore

The example configuration provided in the [Dockstore.json](Dockstore.json) file includes the required input arguments 
`run_id`, which defines the sample identifier, and the input VCFs defined in the arguments `snowman`, `delly`, `brass` and `dranger`.
The other arguments are set to arbitrary output files in the /tmp/ folder.

### Run dockstore cli locally:

```
dockstore tool launch --entry registry.hub.docker.com/weischenfeldt/pcawg_sv_merge:1.0.2 --json Dockstore.json
```

### Run dockstore cli on local cwl:
```
dockstore tool launch --entry Dockstore.cwl --local-entry --json Dockstore.json
```

### Run Dockstore remote cwl:


## Without Dockstore

### Command line interface

Within the docker container, the following command is set up to run the sv merge task::

```
run_sv_merge.py  -h
usage: run_sv_merge.py [-h] --run-id RUN_ID --delly DELLY --snowman SNOWMAN
                       --derenger DERENGER --brass BRASS

Merge various sv caller

optional arguments:
  -h, --help           show this help message and exit
  --run-id RUN_ID      Sample id, identifier of the run
  --delly DELLY        DELLY output VCF
  --snowman SNOWMAN    SNOWMAN output VCF
  --dranger DRANGER  DRANGER output VCF
  --brass BRASS        BRASS output VCF
```

### From the docker run interface

The simpliest example is set up when all the input vcf files are in the same folder, eg: `/your/input_dir`, the docker command can be called as:


```
docker run -ti \
    -v /your/output_dir:/home/docker \
    -v /your/input_dir:/data \
    pcawg_sv_merge:latest run_sv_merge.py \
    --run-id test_run \
    --delly /data/delly_sv_file.vcf.gz \
    --snowman /data/snowman_sv_file.vcf.gz \
    --dranger /data/dranger_sv_file.vcf.gz \
    --brass /data/brass_sv_file.vcf.gz
```

This command execute the merge scripts inside the docker images, and save the results in the `/your/output_dir` directory
